$(document).ready(function() { 

    var baseUrl = atob(sessionStorage.getItem('ip')) +"api", userId = atob(sessionStorage.getItem('userId'));
    var date = atob(sessionStorage.getItem('date')), auth = atob(sessionStorage.getItem('auth')); 
    var bUrl = bUrl = atob(sessionStorage.getItem('ip'));
   
   $('#btnLogin').click(function(){
        $('#btnLogin').attr("disabled","disabled");
        auth = getToken();
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Login',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth,
                'Access-Control-Allow-Origin' : '*'
            },
            data: JSON.stringify({ 
                Username: $('#username').val(), 
                Password: $('#password').val() 
            }),
            success: function(response) {
                displayMsg("success", "Login", "Welcome "+ response.User.FullName + " Login date: " + response.date);
                sessionStorage.setItem('userId', btoa(response.User.UserId));
                sessionStorage.setItem('companyId', btoa(response.User.CompanyId));
                sessionStorage.setItem('auth', btoa(response.access_token));
                setInterval(function(){ window.location.href = "Dashboard" }, 1000);
            },
            error: function(jqXHR){ 
                displayMsg("warning", "Opp!", jqXHR.responseText);
                $('#btnLogin').removeAttr("disabled");
            }
        });
    });

    $('#Reset').click(function(){
        $('#Reset').attr("disabled","disabled"); 
        auth = getToken();
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Reset/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({ 
                Username: $('#Email').val()
            }),
            success: function(response) {
                displayMsg("success", response.Message, response.Output); $('#Email').val('');
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#Reset').removeAttr("disabled");
            },
        });
    });

    $('#btnChangePassword').click(function(){
        $('#btnChangePassword').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/ChangePassword/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Username: $('#Usename').val(), 
                OldPassword: $('#OldPass').val(),
                NewPassword: $('#NewPass').val()
            }),
            success: function(response) {
                displayMsg("success", "Successfull", 'Password has been Change Successfully');
                $('#OldPass').val(''); $('#NewPass').val(''); $('.Close').click();
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#btnChangePassword').removeAttr("disabled");
            },
        });
    });

    $('#btnUpload').click(function(){
        $('#btnUpload').attr("disabled","disabled");
        var pass = $('#Passport').get(0).files; var ptype = 'User';
        var pload = fileUpload(ptype, pass); //console.log(pload);
        $.ajax({
            type: 'POST',
            url: baseUrl + '/ProfileImage/'+userId,
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                ProfileImage: pload
            }),
            success: function(response) {
                displayMsg("success", "Successfull", 'Profile Image has been Change Successfully');
                $('#Passport').val(''); $('#btnUpload').removeAttr("disabled"); $('.Close').click();
                //$('#UserPic').attr("src", bUrl+"Files/User/"+pload);
                $('#UserPic').attr('src', bUrl+"Files/User/"+pload).width(90).height(90);
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#btnUpload').removeAttr("disabled");
            },
        });
    });

    $('#btnStaff').click(function(){
        $('#btnStaff').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Staffs/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Destination: $('#Phone').val(),
                Message: $('#Message').val(),
                Count: parseInt($('#Count').val()),
                AppUserId: parseInt(userId),
                CreatedDate: date
            }),
            success: function(response) {
                displayMsg("success", "Successfull", $('#Message').val()+' has been Sent Successfully');
                setInterval(function(){ window.location.reload() }, 2000);
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#btnStaff').removeAttr("disabled");
            },
        });
    });

    $('#Send').click(function(){
        $('#Send').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Sms/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Destination: $('#Phone').val(),
                Message: $('#Message').val(),
                Count: parseInt($('#Count').val()),
                AppUserId: parseInt(userId),
                CreatedDate: date
            }),
            success: function(response) {
                displayMsg("success", "Successfull", $('#Message').val()+' has been Sent Successfully');
                setInterval(function(){ window.location.reload() }, 2000);
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#Send').removeAttr("disabled");
            },
        });
    });

    $('#Schedule').click(function(){
        $('#Schedule').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Schedules/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Destination: $('#Phone').val(),
                Message: $('#Message').val(),
                Date: $('#SendDate').val(),
                Status: "Pending",
                Count: parseInt($('#Count').val()),
                AppUserId: parseInt(userId),
                CreatedDate: date
            }),
            success: function(response) {
                displayMsg("success", "Successfull", $('#Message').val()+' has been Schedule Successfully');
                setInterval(function(){ window.location.reload() }, 2000);
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#Schedule').removeAttr("disabled");
            },
        });
    });

    $('#btnSendBulk').click(function(){
        $('#btnSendBulk').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Sms/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Destination: $('#Display').val(),
                Message: $('#SmsBody').val(),
                Count: parseInt($('#Count').val()),
                AppUserId: parseInt(userId),
                CreatedDate: date
            }),
            success: function(response) {
                displayMsg("success", "Successfull", $('#SmsBody').val()+' has been Sent Successfully');
                setInterval(function(){ window.location.reload() }, 2000);
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#btnSendBulk').removeAttr("disabled");
            },
        });
    });

    $('#btnSendSingle').click(function(){
        $('#btnSendSingle').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Sms/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Destination: $('#Mobile').val(),
                Message: $('#Message').val(),
                AppUserId: parseInt(userId),
                CreatedDate: date
            }),
            success: function(response) {
                displayMsg("success", "Successfull", $('#SmsBody').val()+' has been Sent Successfully');
                setInterval(function(){ window.location.reload() }, 2000);
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#btnSendSingle').removeAttr("disabled");
            },
        });
    });

    $('#btnSend').click(function(){
        $('#btnSend').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Schedules/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Destination: $('#SDisplay').val(),
                Message: $('#SSmsBody').val(),
                Date: $('#SendDate').val(),
                Status: "Pending",
                Count: parseInt($('#Count').val()),
                AppUserId: parseInt(userId),
                CreatedDate: date
            }),
            success: function(response) {
                displayMsg("success", "Successfull", $('#SSmsBody').val()+' has been Schedule to Send on '+$('#SendDate').val());
                setInterval(function(){ window.location.reload() }, 2000);
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#btnSend').removeAttr("disabled");
            },
        });
    });

    $('#btnSave').click(function(){
        $('#btnSave').attr("disabled","disabled");
        $.ajax({
            type: 'POST',
            url: baseUrl + '/Message/',
            contentType: 'application/json-patch+json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            data: JSON.stringify({
                Type: $('#BType').val(),
                Messages: $('#BSmsBody').val(),
                CompanyId: parseInt(companyId),
                BranchId: parseInt(branchId),
                AppUserId: parseInt(userId),
                CreatedDate: date
            }),
            success: function(response) {
                displayMsg("success", "Successfull", 'Has been Save Successfully');
                setInterval(function(){ window.location.reload() }, 2000);
                $('#btnSave').removeAttr("disabled");
            },
            error: function(jqXHR){
                displayMsg("warning", "Not Successfull", jqXHR.responseText);
                $('#btnSave').removeAttr("disabled");
            },
        });
    });

    function getToken(){
        $.ajax({
            type: 'GET',
            async: false,
            url: baseUrl+'/AccessToken/',
            dataType: 'json',
            success: function(data){
                auth = data.access_token;
            }
        });
      return auth;
    }

    function displayMsg(type, title, text){
        var type = type, title = title, text = text;
        swal({
            title: title,
            text: text,
            type: type
        });
    }

    function fileUpload(type, file){
        var name, fil = file; var seq = type; 
        var data = new FormData();
        console.log(seq); console.log(fil);
        if (fil.length > 0) {
           data.append("file", fil[0]);
        }
        $.ajax({
            type: "POST",
            url: baseUrl +"/Upload/"+ seq,
            contentType: false,
            processData: false,
            async: false,
            headers: {
                'Access-Control-Allow-Origin' : '*',
                'Authorization' : 'Bearer ' + auth
            },
            data: data,
            success: function(jqXHR) {
                name = jqXHR;
                //console.log(jqXHR);
            },
        });
      return name;
    }

});
