<?php

Abstract class Routing
{
    private static $routes = array("forgot_password","login","register","dashboard","staff","customer","alerts","messages","smslogs", "profile", "blogs");

    public static function checkRoute()
    {
        if(empty($_GET['route']))
        {
            $_GET['route'] = "login";
        }
        $_GET['title'] = explode('/', $_GET['route'])[0];
        return (in_array($_GET['title'], self::$routes)) 
                ? $_GET['title'] 
                : "404";
    }
}

?>